
from typing import Dict, List

import numpy as np
import torch
from torch_geometric.data import Data
from mendeleev import element

from atomic_structure_graphs import AtomicStructureGraphs
from cell import Cell
from device import device
from features import Features
from periodic_structure_utils import read_structure
from structure_utils import get_dihedral_angle


class PBCGraphs(AtomicStructureGraphs):

    """

    A class to read structural information of a periodic system
    and convert it to torch_geometric Data graph. In this class, graphs
    are constructed in a chemically intuitive way: a node (atom) has edges
    only to other nodes that are at a distance that is up to alpha times the
    sum of their respective covalent radii away, where alpha is
    a factor >= 1 (default 1.1). In this mode edges will correspond
    to chemical bonds. Covalent radii are extracted from Mendeleev for
    each listed species.

    """

    def __init__(
        self,
        species_list: List[str], 
        edge_features: Features,
        bond_angle_features: Features,
        dihedral_features: Features = None,
        node_feature_list: List[str] = ['atomic_number'],
        n_total_node_features: int = 10,
        n_max_neighbours: int = 6,
        pooling: str = "add",
        alpha: float = 1.1
    ) -> None:

        # initialise the base class

        super().__init__(
           species_list,
           edge_features,
           bond_angle_features,
           dihedral_features,
           node_feature_list,
           n_total_node_features,
           pooling
        )

        self.edge_features = edge_features
        self.bond_angle_features = bond_angle_features
        self.dihedral_features = dihedral_features
        self.n_max_neighbours = n_max_neighbours
        self.alpha = alpha  # alpha is the scaling factor for bond (edge)
        # critera, i.e. two atoms are bonded if their
        # separation is r <= alpha*(rc1 + rc2), where
        # rci are the respective covalent radii

        self.cell_indices = self.get_cell_indices()
        self.covalent_radii = self.get_covalent_radii()

    def get_cell_indices(self) -> np.ndarray:
        """ Sets up a list of neighbouring cell indices for periodic boundary conditions """

        indices = np.zeros((27,3), dtype = float)

        for i in range(27):

            ix = float(i % 3 - 1)
            iy = float(( i // 3 ) % 3 - 1)
            iz = float(i // 9 - 1)

            indices[i,:] = np.array([ix,iy,iz])

        return indices

    def get_covalent_radii(self) -> Dict[str, float]:

        """

        Sets up and returns a dictionary of covalent radii (in Ang)
        for the list of species in its argument

        :return: covalent_radii: dict of covalent radius for eash species (in Angstrom)
        :rtype: dict

        """

        covalent_radii = {}

        for label in self.species:

            spec = element(label)

            covalent_radii[label] = spec.covalent_radius / 100.0
            # mendeleev stores radii in pm, hence the factor

        return covalent_radii

    def structure2graph(self, fileName: str) -> Data:

        """

        A function to read molecule information from the database file and
        convert it to torch_geometric Data graph. In this particular class
        graphs are constructed in the following way:

        Chemically intuitive way: a node (atom) has edges only to
           other nodes that are at a distance that is up to alpha times the
           sum of their respective covalent radii away. In this mode
           edges will correspond to chemical bonds. To activate this
           mode it is necessary to pass the dictionary covalent_radii;
           if it is not passed or is set to None, the second mode is
           activated (see below).

        Args:

        :param: fileName (string): the path to the file where the molecule
           information is stored in file.
        :type: str
        :return: graph representation of the molecule contained in fileName
        :rtype: torch_geometric.data.Data

        """

        (
            cell,
            labels,
            positions,
            properties
        ) = read_structure(fileName)

        # the total number of node features is given by the species features

        n_atoms, _ = positions.shape

        n_features = self.spec_features[labels[0]].size + \
                     self.bond_angle_features.n_features()
        node_features = torch.zeros((n_atoms, n_features), dtype=torch.float32)

        # atoms will be graph nodes; edges will be created for every
        # neighbour of i that is among the nearest
        # n_max_neighbours neighbours of atom i

        # in what follows we assume that the cell dimensions are large 
        # compared to typical edge (bond) distances, so that we only
        # need to consider neighbouring cells to find all edge connections
        # in the graph

        n_max = 14 # expected maximum number of neighbours bonded to node
        n_neighbours = np.zeros((n_atoms), dtype=int)
        neighbour_distance = np.zeros((n_atoms, n_max), dtype=float)
        neighbour_cell = np.zeros((n_atoms, n_max, 3), dtype=float)
        neighbour_index = np.zeros((n_atoms, n_max), dtype=int)

        node0 = []
        node1 = []
        edge_length = []

        for i in range(n_atoms - 1):

            rc_i = self.covalent_radii[labels[i]]

            si = positions[i,:] - np.floor( positions[i,:] )

            for j in range(i + 1, n_atoms):

                rc_j = self.covalent_radii[labels[j]]

                sj = positions[j,:] - np.floor( positions[j,:] )

                dcut = self.alpha * ( rc_i + rc_j ) 

                dcut2 = dcut * dcut

                """
  
                The brute force way of looking for neighbours is just too expensive
                Let us try using the nearest-image convention

                for i_cell in range(27):

                    sj_image = sj + self.cell_indices[i_cell,:]

                    rij2 = cell.squared_distance(sj_image, si)
                            
                    if rij2 <= dcut2:

                       node0.append(i)
                       node1.append(j)
   
                       node0.append(j)
                       node1.append(i)

                       dij = np.sqrt(rij2)

                       neighbour_distance[i, n_neighbours[i]] = dij
                       neighbour_distance[j, n_neighbours[j]] = dij

                       edge_length.append(dij)  # two lengths, because graph
                       edge_length.append(dij)  # is undirected

                       neighbour_cell[i, n_neighbours[i], :] = \
                          self.cell_indices[i_cell,:]
                       neighbour_cell[j, n_neighbours[j], :] = \
                         -self.cell_indices[i_cell,:]

                       neighbour_index[i, n_neighbours[i]] = j
                       neighbour_index[j, n_neighbours[j]] = i

                       n_neighbours[i] += 1
                       n_neighbours[j] += 1

                """
 
                indices = np.round( sj - si )  # indices of box in which j is closest to i

                sj_image = sj - indices

                rij2 = cell.squared_distance( sj_image, si )

                if rij2 <= dcut2:

                   node0.append(i)
                   node1.append(j)
  
                   node0.append(j)
                   node1.append(i)

                   dij = np.sqrt(rij2)

                   neighbour_distance[i, n_neighbours[i]] = dij
                   neighbour_distance[j, n_neighbours[j]] = dij

                   edge_length.append(dij)  # two lengths, because graph
                   edge_length.append(dij)  # is undirected

                   neighbour_cell[i, n_neighbours[i], :] = -indices
                   neighbour_cell[j, n_neighbours[j], :] = indices

                   neighbour_index[i, n_neighbours[i]] = j
                   neighbour_index[j, n_neighbours[j]] = i

                   n_neighbours[i] += 1
                   n_neighbours[j] += 1

        edge_index = torch.tensor([node0, node1], dtype=torch.long)

        _, num_edges = edge_index.shape

        # construct node geometric features; these will be appended at the end
        # of the features that are purely related to the species

        n_ba_features = self.bond_angle_features.n_features()

        for i in range(n_atoms):

            anglehist = np.zeros((n_ba_features), dtype=float)

            si = positions[i,:] - np.floor(positions[i,:])

            for n in range(n_neighbours[i] - 1):

                j = neighbour_index[i, n]

                sj = positions[j,:] - np.floor(positions[j,:])

                sij = sj + neighbour_cell[i,n,:] - si

                rij = cell.to_Cartesian( sij )

                dij = neighbour_distance[i, n]

                for m in range(n + 1, n_neighbours[i]):

                    k = neighbour_index[i, m]

                    sk = positions[k,:] - np.floor(positions[k,:])

                    sik = sk + neighbour_cell[i,m] - si
           
                    rik = cell.to_Cartesian( sik )

                    dik = neighbour_distance[i, m]

                    costhetaijk = np.dot(rij, rik) / (dij * dik)

                    anglehist += self.bond_angle_features.u_k(costhetaijk)

            node_total_features = np.concatenate(
               ( 
                   self.spec_features[labels[i]], anglehist 
               ) 
            )

            node_features[i, :] = torch.from_numpy(node_total_features)

        # now, based on the edge-index information, we can construct the edge attributes

        bond_features = []

        for n in range(num_edges):

            i = edge_index[0, n]
            j = edge_index[1, n]

            dij = edge_length[n]

            bond_features.append(self.edge_features.u_k(dij))

        # in periodic systems we are not going to allow for the time being
        # the use of dihedral features, as these complicate things excessively

        # if self.dihedral_features:

        #     n_da_features = self.dihedral_features.n_features()

        #     for n in range(num_edges):

        #         i = edge_index[0, n]
        #         j = edge_index[1, n]

        #         ri = positions[i, :]
        #         rj = positions[j, :]

        #         rij = rj - ri

        #         anglehist = np.zeros((n_da_features), dtype=float)

        #         for ni in range(n_neighbours[i]):

        #             k = neighbour_index[i, ni]

        #             if k == j:
        #                 continue  # k must be different from j

        #             rki = ri - positions[k, :]

        #             for nj in range(n_neighbours[j]):

        #                 l = neighbour_index[j, nj]

                        # cannot define a dihedral if l == k, or l == i
        #                 if l in (k, i):
        #                     continue

        #                 rjl = positions[l, :] - rj

                        # check if vectors are colinear

        #                vec = np.cross( (rij-rki), (rjl-rij) )

        #                if np.dot(vec,vec) > 0.0:

        #                   costhetaijkl = get_dihedral_angle(rki, rij, rjl)
        #                   anglehist += self.dihedral_features.u_k(costhetaijkl)

        #        total_bond_features = np.concatenate((bond_features[n], anglehist))

        #        bond_features[n] = total_bond_features

        # it is apparently faster to convert numpy arrays to tensors than
        # to convert arrays of numpys, so let's do it this way

        features = np.array(bond_features)

        # now we can create a graph object (Data)

        edge_attr = torch.tensor(features, dtype=torch.float32)

        if self.pooling == "add":
            y = torch.tensor(properties, dtype=torch.float32)
        else:
            y = torch.tensor((properties / float(n_atoms)), dtype=torch.float32)

        pos = torch.from_numpy(positions)

        crystal_graph = Data(
            x=node_features.to(device), 
            y=y.to(device), 
            edge_index=edge_index.to(device), 
            edge_attr=edge_attr.to(device), pos=pos
        )

        # we do not put pos in device at the moment, since this is not needed in the fitting

        return crystal_graph

# register this derived class as subclass of AtomicStructureGraphs

AtomicStructureGraphs.register(PBCGraphs)
