import numpy as np
import torch
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
from glob import glob

import EdgeFeatureFunction as eff

def getForces( model: torch.nn.Module, graph: Data ) -> np.ndarray:

    """
    Given a model to evaluate energy and a graph representing a molecule, it returns the forces

    :param model: a torch model representing this system
    :type: torch.nn.Module
    :param: graph, the graph for which forces are to be calculated 

    :return: forces 
    :rtype: np.ndarray

    """

    _, num_edges = graph.edge_index.shape

    nAtoms, _ = graph.pos.shape

    batch = torch.zeros( nAtoms, dtype = torch.int64 )

    graph.edge_attr.requires_grad_( requires_grad = True )

    energy = model( graph.x, graph.edge_index, graph.edge_attr, batch )

    energy.backward( retain_graph = True )  # do we need retain_graph = True here? 
    # energy.backward()  # do we need retain_graph = True here? 

    dededge = graph.edge_attr.grad

    force = torch.zeros( ( nAtoms, 3 ), dtype = float )

    for n in range( num_edges ):

        i = graph.edge_index[0,n]
        j = graph.edge_index[1,n]

        rij = eff.edgeFeatureInverse( graph.edge_attr[n].item() )

        dEdf = dededge[n,0]
        dEdr = dEdf * eff.edgeFeatureDerivative( rij )

        fij = dEdr * ( graph.pos[j,:] - graph.pos[i,:] ) / rij

        force[i,:] += fij
        force[j,:] -= fij
 
    return force
