
""" This function finds, given two clusters, the rotation and translation operations that achieve maximum 
    superposition of both structures """

import numpy as np
from scipy.linalg import svd, diagsvd, det

from Cluster import Cluster

def Procrustes( cluster, clusterRef ):
    """ find the rotation and translation that maximally overlaps cluster onto clusterRef """

    # first, for each cluster, find the centre (similar to centre-of-mass, but without mass weighting)

    nAtoms = min( cluster.nAtoms, clusterRef.nAtoms )  # we take only the 

    atoms = np.zeros( ( nAtoms, 3 ) )
    atomsRef = np.zeros( ( nAtoms, 3 ) )

    mu = np.zeros( 3 )
    muRef = np.zeros( 3 )

    for n in range( nAtoms ):
        if n < cluster.nAtoms:
           v1 = cluster.atoms[n].coordinates
           atoms[n,:] = v1
           mu += v1
        if n < clusterRef.nAtoms:
           v2 = clusterRef.atoms[n].coordinates
           atomsRef[n,:] = v2
           muRef += v2

    mu /= float( nAtoms )
    muRef = float( nAtoms )

    # now we displace each cluster so that they have a common centre

    for n in range( nAtoms ):
        atoms[n,:] -= mu
        atomsRef[n,:] -= muRef

    M = np.dot( atomsRef.T, atoms )

    # now use SVD to find rotation 

    U, s, Vh = svd( M )

    R = np.dot( U, Vh )   # the transformation matrix

    # check if this is a proper rotation matrix (detR == 1)

    isRotation = False

    determinant = det( R )

    if np.fabs( determinant - 1.0 ) <= 1.0e-5:
       isRotation = True

    return R, isRotation
