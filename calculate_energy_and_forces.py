import torch
from torch import Tensor
from torch_geometric.data import Data, Batch

# import EdgeFeatureFunction as eff
# above line needs to be substituted and force calculation updated

# TODO: correct & complete calculation of forces; we need to loop over edge features

def calculate_energy_and_forces( model: torch.nn.Module, sample: Data, batch: Batch ) -> Tensor:

    r""" Given a model and a graph (or batch thereof), calculate energy and forces """

    nAtoms = sample.num_nodes
    nEdges = sample.num_edges

    forces = torch.zeros( ( nAtoms, 3 ), dtype = torch.float32 )

    sample.edge_attr.requires_grad_( requires_grad = True )

    energy = model( sample.x, sample.edge_index, sample.edge_attr, batch )

    energy.sum().backward( retain_graph = True )  # do we need retain_graph = True here? 
         # energy.backward()  # do we need retain_graph = True here? 

    dEdedge = sample.edge_attr.grad

    for m in range( nEdges ):

        i = sample.edge_index[0,m]
        j = sample.edge_index[1,m]

        rij = eff.edgeFeatureInverse( sample.edge_attr[m].item() )

        dEdf = dEdedge[m,0]
        dEdr = dEdf * edgeFeatureDerivative( rij )

        fij = dEdr * ( sample.pos[j,:] - sample.pos[i,:] ) / rij

        forces[i,:] += fij
        forces[j,:] -= fij

    return energy, forces
