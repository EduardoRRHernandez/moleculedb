
import sys
from typing import List

import numpy as np
import torch
from torch_geometric.data import Data
from torch_geometric.utils import to_undirected

from atomic_structure_graphs import AtomicStructureGraphs
from features import Features
from QM9_utils import read_QM9_structure
from structure_utils import get_dihedral_angle

class GeometricMolecularGraphs(AtomicStructureGraphs):

    """

    A class to read molecule information from the database file
    and convert it to torch_geometric Data graph. In this class, graphs
    are constructed 'geometrically', i.e.: each node will have a
    minimum of n_max_neighbours nearest nodes; this is a minimum
    number because the graph should be undirected, meaning that if j
    is neighbour of i, i must be a neighbour of j, even if i is not
    among the n_max_neighbours nearest neighbours of j; therefore the
    actual number of neighbours of a node is at least equal
    to n_max_neighbours, but in fact can be slightly larger.

    """

    def __init__(
        self,
        edge_features: Features,
        bond_angle_features: Features,
        dihedral_features: Features = None,
        node_feature_list: List[str] = ['atomic_number'],
        n_total_node_features: int = 10,
        n_max_neighbours: int = 6,
        pooling: str = "add"
    ) -> None:

        """

        Args:

        :param: edge_features: an instance of class Features defining the edge features
        :type: Features
        :param: bond_angle_features: same as above, for bond angle features
        :type: Features
        :param: dihedral_features (Optional): if given, same as above for dihedrals
        :type: Features
        :param: node_feature_list: a list containing physical features of nodes (e.g.)
          atomic_number, covalent_radius, etc; the features must correspond to valid
          mendeleev.Element keys (see `mendeleev <https://mendeleev.readthedocs.io/en/stable/>`_) 
        :type: List[str]
        :param int n_total_node_features: total number of node features (physical node features
          plus extra-ones) 
        :param int n_max_neighbours: maximum number of node neighbours (default = 6)
        :param float alpha: the scale factor to apply to the sum of covalent radii to be used as
          criterion for deciding if two nodes are neighbours (linked by an edge) or not (default 1.1)
        :param str pooling: the type of pooling over nodes to be conducted by the model
          (default "add", could be alternatively set to "mean")

        """

        # initialise the base class

        super().__init__(node_feature_list,
                         n_total_node_features, pooling)

        self.n_max_neighbours = n_max_neighbours

        self.edge_features = edge_features
        self.bond_angle_features = bond_angle_features
        self.dihedral_features = dihedral_features

    def structure2graph(self, fileName: str) -> Data:

        """

        Args:

        :param str  fileName: the path to the file where the molecule
               information is stored in file.
        :returns: a Data object representing the molecule graph
        :rtype: torch_geometric.data.Data

        """

        (
            molecule_id,
            n_atoms,
            labels,
            positions,
            properties,
            charges,
        ) = get_QM9_structure(fileName)

        # the total number of node features is given by the species features

        n_features = self.spec_features[labels[0]].size
        node_features = torch.zeros((n_atoms, n_features), dtype=torch.float32)

        # atoms will be graph nodes; edges will be created for every
        # neighbour of i that is among the nearest
        # n_max_neighbours neighbours of atom i

        # first we loop over all pairs of atoms and calculate the matrix of squared distances

        dij2 = np.zeros((n_atoms, n_atoms), dtype=float)

        largest_r = 0.0
        for i in range(n_atoms - 1):
            for j in range(i + 1, n_atoms):

                rij = positions[j, :] - positions[i, :]
                rij2 = np.dot(rij, rij)

                dij2[i, j] = rij2
                dij2[j, i] = rij2

                if rij2 > largest_r:
                    largest_r = rij2

        largest_r = np.sqrt(largest_r)

        edge_parameters = self.edge_features.parameters()

        if largest_r > edge_parameters["x_max"]:

            print("Some edges are longer than the maximum edge parameter!")
            print("Increase rMax or program will fail!")
            sys.exit()

        # for each atom, select only the nearest n_max_neighbours;
        # this requires sorting the squared distances

        if n_atoms <= self.n_max_neighbours:
            n_max = n_atoms - 1
        else:
            n_max = self.n_max_neighbours

        neighbour_index = np.zeros((n_atoms, n_max), dtype=int)

        for i in range(n_atoms):

            squared_distances = dij2[i, :]
            indices = np.argsort(squared_distances)

            neighbour_index[i, 0:n_max] = indices[1 : n_max + 1]

        # now we are ready to create the graph

        node0 = []
        node1 = []

        for i in range(n_atoms):

            for n in range(n_max):

                j = neighbour_index[i, n]

                node0.append(i)
                node1.append(j)

        # before we can create the graph, we need to make sure that
        # the graph will be undirected, i.e. that
        # if atom j is a neighbour of i, i is a neighbour of j;
        # this is not necessarily the case when fixing
        # the number of neighbours, so we must impose it

        edges = torch.tensor([node0, node1], dtype=torch.long)

        edge_index = to_undirected(edge_index=edges)

        _, num_edges = edge_index.shape

        # finally, to construct the geometric features for nodes and edges we need to know
        # number of neighbours, neighbour indices and distances for each atom

        nNeighbours = np.zeros((n_atoms), dtype=int)
        neighbour_index = np.zeros((n_atoms, n_atoms), dtype=int)
        neighbour_distance = np.zeros((n_atoms, n_atoms), dtype=float)

        for n in range(num_edges):

            i = edge_index[0, n]
            j = edge_index[1, n]

            neighbour_index[i, nNeighbours[i]] = j
            neighbour_distance[i, nNeighbours[i]] = np.sqrt(dij2[i, j])

            nNeighbours[i] += 1

            # we do not repeat for j because graph is undirected
            # and looping over edges takes care of this

        # construct node geometric features; the name is confusing, as these will really
        # be edge features; the node features consist only of species properties

        n_ba_features = self.bond_angle_features.n_features()

        node_geometric_features = np.zeros((n_atoms, n_ba_features))

        for i in range(n_atoms):

            anglehist = np.zeros((n_ba_features), dtype=float)

            for n in range(nNeighbours[i] - 1):

                j = neighbour_index[i, n]

                rij = positions[j, :] - positions[i, :]
                dij = neighbour_distance[i, n]

                for m in range(n + 1, nNeighbours[i]):

                    k = neighbour_index[i, m]

                    rik = positions[k, :] - positions[i, :]
                    dik = neighbour_distance[i, m]

                    costhetaijk = np.dot(rij, rik) / (dij * dik)

                    anglehist += self.bond_angle_features.u_k(costhetaijk)

            node_geometric_features[i, :] = anglehist
            node_features[i, :] = torch.from_numpy(self.spec_features[labels[i]])

        # now, based on the edge-index information, we can construct the edge attributes

        bond_features = []

        for n in range(num_edges):

            i = edge_index[0, n]
            j = edge_index[1, n]

            dij = np.sqrt(dij2[i, j])

            angle_features = node_geometric_features[i,:] + \
                             node_geometric_features[j,:]

            features = np.concatenate(
                (
                    self.edge_features.u_k(dij), angle_features
                )
            )

            bond_features.append(features)

        # finally, if we have attributes for bond angles to be added to edges..

        if self.dihedral_features:

            n_da_features = self.dihedral_features.n_features()

            for n in range(num_edges):

                i = edge_index[0, n]
                j = edge_index[1, n]

                ri = positions[i, :]
                rj = positions[j, :]

                rij = rj - ri

                anglehist = np.zeros((n_da_features), dtype=float)

                for ni in range(nNeighbours[i]):

                    k = neighbour_index[i, ni]

                    if k == j:
                        continue  # k must be different from j

                    rki = ri - positions[k, :]

                    for nj in range(nNeighbours[j]):

                        l = neighbour_index[j, nj]

                        # cannot define a dihedral if l == k, or l == i
                        if l in (k, i):
                            continue

                        rjl = positions[l, :] - rj

                        # check if vectors are colinear

                        vec = np.cross( (rij-rki), (rjl-rij) )

                        if np.dot(vec,vec) > 0.0:

                           costhetaijkl = get_dihedral_angle(rki, rij, rjl)
                           anglehist += self.dihedral_features.u_k(costhetaijkl)

                total_bond_features = np.concatenate((bond_features[n], anglehist))

                bond_features[n] = total_bond_features

        # it is apparently faster to convert numpy arrays to tensors than
        # to convert arrays of numpys, so let's do it this way

        features = np.array(bond_features)

        # calculate the atomisation energy

        molecule_ref = 0.0

        for n in range(n_atoms):

            molecule_ref += self.atom_ref[labels[n]]

        atomisation_energy = self.Hartree2eV * (properties[0, 10] - molecule_ref)

        # now we can create a graph object (Data)

        edge_attr = torch.tensor(features, dtype=torch.float32)

        if self.pooling == "add":
            y = torch.tensor(atomisation_energy, dtype=torch.float32)
        else:
            y = torch.tensor((atomisation_energy / float(n_atoms)), \
                  dtype=torch.float32)

        pos = torch.from_numpy(positions)

        molecule_graph = Data(
            x=node_features.to(device), 
            y=y.to(device), 
            edge_index=edge_index.to(device), 
            edge_attr=edge_attr.to(device), 
            pos=pos
        )

        # we do not put pos in device at the moment, since this is not needed in the fitting

        return molecule_graph

# register this derived class as subclass of MolecularGraphs

AtomicStructureGraphs.register(GeometricMolecularGraphs)
