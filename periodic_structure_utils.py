
import re
from typing import List, Tuple
import yaml

import numpy as np

from cell import Cell

def read_structure(
       file_name: str
    ) -> Tuple[Cell,List,np.ndarray,float]:


    """

    This function reads structural information of a periodic
    system contained in the input file file_name. file_name is a yaml
    file from which we read cell data and atomic positions, which can be
    either in Cartesian coordinates in Angstrom (fractional = False, default),
    or in fractional coordinates. The cell information can be given 
    either as a list of six numbers (a, b, c, alpha, beta, gamma) or 
    else as three vectors, a = [a1, a2, a3], b = [b1, b2, b3], c = [c1, c2, c3]
    [[a1,a2,a3], [b1,b2,b3], [c1,c2,c3]]
    Regardless of how the atomic positions are given, they are returned in 
    fractional coordinates, because for periodic systems this is the most
    practical way of calculating connections in the structure graph.

    Args:

    :param str file_name: filename containing the molecular information

    :return: cell, a Cell object describing the simulation cell.
    :return: species, the chemical species of each atom
    :return: positions, np.array containing the fractional coordinates of the atoms
    :return: properties, np.array containing the properties to be used in fitting 
             (typically the total energy)

    """

    with open(file_name, "r") as file_in:
        input_data = yaml.load(file_in, Loader=yaml.Loader)

    # first we are going to read the cell parameters

    cell_parameters = input_data.get('cell') 

    cell = Cell( cell_parameters )

    # then the properties to be used for fitting

    value = input_data.get('property')

    prop = float(value)

    # are coordinates given in fractional form?

    fractional = input_data.get('fractional', False )

    # finally read the coordinates

    atoms = input_data.get('atoms')

    n_atoms = len( atoms )
    species = []
    positions = np.zeros((n_atoms,3), dtype=float)

    # in what follows we go atom by atom storing the species and coordinates
    # if positions are in Cartesian, convert them to fractional 

    tmatrix = np.zeros((3,3), dtype = float)
    tmatrix[:,0] = cell.reciprocal_a()
    tmatrix[:,1] = cell.reciprocal_b()
    tmatrix[:,2] = cell.reciprocal_c()

    for n, line in enumerate(atoms):

        words = line.split()

        species.append(words[0])

        vec = np.array([float(words[1]), float(words[2]), float(words[3])])

        if not fractional:

           pos = np.dot( tmatrix, vec )

           positions[n,:] = pos

        else:

           positions[n,:] = vec

    return cell, species, positions, prop

