# MoleculeDB


## Name
MoleculeDB

## Description
This project implements a Convolutional Graph Neural Network model to predict energies, and eventually 
forces, of small organic molecules containing H, C, N, O and/or F. 

The model consists of a number of GNN convolutional layers, followed by a dense NN that outputs an atomic
contribution to the total energy; all atomic (node) contributions are summed to output a total energy.

Each node represents an atom; edges are set up between each node (atom) and its nMaxNeighbours nearest 
neighbours, where the default is 12. 

The node features can be anything that makes sense, such as atomic_number, covalent_radius, vdw_radius, 
electron_affinity, en_pauling (Pauling electronegativity scale), and in general anything that is 
included in the mendeleev python database for chemical species. At present these features are not 
normalised in any way, but it may prove desirable to normalise them. 

The physical node features are complemented by abstract features, currently a vector of real values 
consisting of an oscillating function whose amplitude depends on the element's period number, and frequency 
on its group number. V(x) = 0.1 * prediod_number * sin( pi * x * group_number )

At present the code fits a GCN model to reproduce energies of molecules in the QM9 dataset; eventually
this could be extended to other databases for e.g. crystalline materials. 

## Dependencies

The code depends on pytorch and pytorch_geometric; other dependencies are mendeleev, scipy, numpy 
and matplotlib.pyplot

## Running the code

After the QM9 datafiles have been downloaded, and split into train, test and validate sets in 
separate subdirectories, you simply run the code like this:

python fit_graph.py input.yml

Here input.yml is a yaml file containing the configuration parameters for the run; a sample
input file is provided (sample.yml).

## Authors and acknowledgment
Eduardo Hernandez (Eduardo.Hernandez@csic.es)

## License

MIT License

Copyright (c) 2022 Eduardo Hernández (Eduardo.Hernandez@csic.es)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## Project status
Exploratory
