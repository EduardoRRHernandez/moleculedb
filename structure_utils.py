
import numpy as np

# some useful functions for structure calculations

def get_dihedral_angle(
        rki: np.ndarray, rij: np.ndarray, rjl: np.ndarray
) -> float:

    """

    A function to compute the dihedral angle 
    defined by three vectors;
    given vectors rki, rij, rjl, defined in the
    moiety k-i-j-l, it returns the cosine of the dihedral angle

    Args:

    :param np.ndarray rki: ri - rk
    :param np.ndarray rij: rj - ri
    :param np.ndarray rjl: rl - rj

    """

    # WARNING: Avoid using np.cross: this can cause a numerical error
    # by sometimes resulting in abs(cosphi)= 1 + delta, delta ~ 1.0e-16
    # which is sufficient to cause np.arccos to fail.
    # vkiij = np.cross( rki, rij )
    vkiij0 = rki[1] * rij[2] - rki[2] * rij[1]
    vkiij1 = rki[2] * rij[0] - rki[0] * rij[2]
    vkiij2 = rki[0] * rij[1] - rki[1] * rij[1]

    vkiij = np.array([vkiij0, vkiij1, vkiij2], dtype=float)

    nkiij = np.sqrt(np.dot(vkiij, vkiij))

    # vijjl = np.cross( rij, rjl )
    vijjl0 = rij[1] * rjl[2] - rij[2] * rjl[1]
    vijjl1 = rij[2] * rjl[0] - rij[0] * rjl[2]
    vijjl2 = rij[0] * rjl[1] - rij[1] * rjl[0]

    vijjl = np.array([vijjl0, vijjl1, vijjl2], dtype=float)

    nijjl = np.sqrt(np.dot(vijjl, vijjl))

    cosphi = np.dot(vkiij, vijjl) / (nkiij * nijjl)

    return cosphi

