
from typing import List, Tuple, Union

import numpy as np

class Cell:

    """
    Defines the Cell class, a class to describe simulation cells in PBC

    """

    def __init__( 
        self, 
        cell_parameters: Union[List,np.ndarray],
        radians: bool = False, 
    ) -> None:

       """
       Initialises the Cell object
        
       Args:

       :param cell_parameters: can be given either as a list of reals or
          a numpy array. If it is a 1D list/array of length 6 it is assumed to contain
          (length_a, length_b, length_c, alpha, beta, gamma) in this order, 
          where the lengths are in Angstrom, the angles in degrees, and 
          alpha is the angle between b and c, beta between a and c, gamma
          between a and b. Alternatively, it should be a (3,3) array, such that
          a = cell_parameters[:,0], b = cell_parameters[:,1], etc

       :param radians: (bool, False) if some of the cell parameters are cell
          angles, by default they will be in degrees, but if radians = True they will
          be read in radians.
    
       """

       if isinstance(cell_parameters, list):

          cell_parameters = np.array( cell_parameters )

       if cell_parameters.size == 6:

          self._length_a = cell_parameters[0]
          self._length_b = cell_parameters[1]
          self._length_c = cell_parameters[2]

          self._alpha = np.pi * cell_parameters[3] / 180.  # angles are given in 
          self._beta = np.pi * cell_parameters[4] / 180.   # degrees, so convert
          self._gamma = np.pi * cell_parameters[5] / 180.  # to radians

          if radians: 

             self._alpha = cell_parameters[3]
             self._beta = cell_parameters[4]
             self._gamma = cell_parameters[5]

          g00 = self._length_a * self._length_a
          g11 = self._length_b * self._length_b
          g22 = self._length_c * self._length_c

          g01 = np.cos( self._gamma ) * np.sqrt( g00 * g11 )
          g02 = np.cos( self._beta ) * np.sqrt( g00 * g22 )
          g12 = np.cos( self._alpha ) * np.sqrt( g11 * g22 )

          self._a = np.zeros(3)
          self._b = np.zeros(3)
          self._c = np.zeros(3)

          self._a[0] = np.sqrt( g00 )

          self._b[0] = g01 / np.sqrt( g00 )
          self._b[1] = np.sqrt( g11 - g01 * g01 / g00 )

          self._c[0] = g02 / np.sqrt( g00 )
          self._c[1] = ( g00 * g12 - g01 * g01 ) / \
                 np.sqrt( g00 * g00 * g11 - g00 * g01 * g01 )
          self._c[2] = np.sqrt( g22 - self._c[0] * self._c[0] - \
                               self._c[1] * self._c[1] )

          self._metric_tensor = np.zeros((3,3))
  
          self._metric_tensor[0,0] = g00
          self._metric_tensor[1,1] = g11
          self._metric_tensor[2,2] = g22

          self._metric_tensor[0,1] = self._metric_tensor[1,0] = g01
          self._metric_tensor[0,2] = self._metric_tensor[2,0] = g02
          self._metric_tensor[1,2] = self._metric_tensor[2,1] = g12

       elif cell_parameters.size == 9:

          self._a = cell_parameters[:,0]
          self._b = cell_parameters[:,1]
          self._c = cell_parameters[:,2]

          self._length_a = np.sqrt( np.dot( self._a, self._a ) )
          self._length_b = np.sqrt( np.dot( self._b, self._b ) )
          self._length_c = np.sqrt( np.dot( self._c, self._c ) )

          cos_angle = np.dot( self._b, self._c ) / \
                     ( self._length_b * self._length_c )

          self._alpha = np.arccos( cos_angle )

          cos_angle = np.dot( self._a, self._c ) / \
                     ( self._length_a * self._length_c )

          self._beta = np.arccos( cos_angle )

          cos_angle = np.dot( self._a, self._b ) / \
                     ( self._length_a * self._length_b )

          self._gamma = np.arccos( cos_angle )

          self._metric_tensor = np.zeros((3,3))

          g01 = np.dot( self._a, self._b )
          g02 = np.dot( self._a, self._c )
          g12 = np.dot( self._b, self._c )

          self._metric_tensor[0,0] = np.dot( self._a, self._a )
          self._metric_tensor[1,1] = np.dot( self._b, self._b )
          self._metric_tensor[2,2] = np.dot( self._c, self._c )

          self._metric_tensor[0,1] = self._metric_tensor[1,0] = g01
          self._metric_tensor[0,2] = self._metric_tensor[2,0] = g02
          self._metric_tensor[1,2] = self._metric_tensor[2,1] = g12

       # here we calculate the cell volume

       vec = np.cross( self._b, self._c )

       self._volume = np.dot( vec, self._a )

       # we also need the vectors of the reciprocal lattice modulo 2.pi,
       # so that we can transform back-forth between fractional and 
       # Cartesian coordinates

       self._ra = np.cross( self._b, self._c ) / self._volume
       self._rb = np.cross( self._c, self._a ) / self._volume
       self._rc = np.cross( self._a, self._b ) / self._volume

       # the following are the transformation matrices from 
       # fractional to Cartesian coordinates and the reverse

       self._h = np.array( [self._a, self._b, self._c] ).T
       self._rh = np.array( [self._ra, self._rb, self._rc] )

    def to_Cartesian( self, s_i: np.ndarray ) -> np.ndarray:
       """ Transform fractional to Cartesian coordinates. """
 
       return np.dot( self._h, s_i )

    def to_fractional( self, r_i: np.ndarray ) -> np.ndarray:
       """ Transform Cartesian to fractional coordinates. """

       return np.dot( self._rh, r_i )

    def squared_distance( self, x_j, x_i ) -> float:
       """ Returns the squared distance between two points given in fractional coordinates """

       sij = x_j - x_i

       rij2 = np.dot( sij, np.dot( self._metric_tensor, sij ) )

       return rij2

    def alpha( self, radians: bool = False ) -> float:
       """ Return alpha (default in degrees.) """

       factor = 180. / np.pi

       if radians: factor = 1.0

       angle = self._alpha * factor

       return angle

    def beta( self, radians: bool = False ) -> float:
       """ Return beta (default in degrees.) """

       factor = 180. / np.pi

       if radians: factor = 1.0

       angle = self._beta * factor

       return angle

    def gamma( self, radians: bool = False ) -> float:
       """ Return gamma (default in degrees.) """

       factor = 180.0 / np.pi

       if radians: factor = 1.0 

       angle = self._gamma * factor

       return angle

    def angles( self, radians: bool = False ) -> Tuple[float]:
       """ Return all three angles in a tuple """

       factor = 180.0 / np.pi
 
       if radians: factor = 1.0 

       alpha, beta, gamma = factor * self._alpha, \
                            factor * self._beta, \
                            factor * self._gamma

       return alpha, beta, gamma

    def length_a( self ) -> float:

       return self._length_a

    def length_b( self ) -> float:

       return self._length_b

    def length_c( self ) -> float:

       return self._length_c

    def lengths( self ) -> Tuple[float]:

       return self._length_a, self._length_b, self._length_c

    def a( self ) -> np.ndarray:

        return self._a

    def b( self ) -> np.ndarray:

        return self._b

    def c( self ) -> np.ndarray:

        return self._c

    def volume( self ) -> float:
       """ Return cell volume. """

       return self._volume

    def metric_tensor( self ) -> np.ndarray:

       return self._metric_tensor

    def reciprocal_a( self ) -> np.ndarray:

       return self._ra

    def reciprocal_b( self ) -> np.ndarray:

       return self._rb

    def reciprocal_c( self ) -> np.ndarray:

       return self._rc
