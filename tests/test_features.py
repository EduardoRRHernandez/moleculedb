
# test_features

from os.path import abspath
import sys

import pytest

path = abspath('.')
sys.path.append(path)

from features import Features, VariableOutOfBounds

def test_out_of_bounds():

    features = Features(x_min = 1., x_max = 2., n_features = 40 )

    with pytest.raises(VariableOutOfBounds):
        features.u_k(0.)
 
    with pytest.raises(VariableOutOfBounds):
        features.u_k(3.)

